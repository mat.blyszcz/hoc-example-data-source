const comments = [
  { id: 0, content: 'Bolek i lolek' },
  { id: 0, content: 'Siurek i kurek' }
];

const blogPost = {
  title: 'Title of the post'
}

const DataSource = {
  addChangeListener: () => {},
  removeChangeListener: () => {},
  getComments: () => comments,
  getBlogPost: () => blogPost
}

export default DataSource;
