import React, { Component } from 'react';
import CommentList from './components/CommentList';
import BlogPost from './components/BlogPost';
import './App.css';


class App extends Component {
  render() {
    return (
      <div className="App">
        <CommentList />
        <hr/>
        <BlogPost />
      </div>
    );
  }
}

export default App;
